Pod::Spec.new do |s|
  s.name             = "PTNetwork"

  s.version          = "0.2.4"

  s.summary          = "PTNetwork base AFNetworking 3.0 and ReactiveCocoa 4.0."

  s.homepage         = "https://bitbucket.org/ptdev/ptnetwork"

  s.license          = 'MIT'
  s.author           = { "PTiOSDev" => "quyx@protruly.com.cn" }
  s.source           = { :git => "git@bitbucket.org:ptdev/ptnetwork.git", :tag => s.version.to_s }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'PTNetwork' => ['Pod/Assets/*.png']
  }

  s.public_header_files = 'Pod/Classes/**/*.h'

  s.dependency 'AFNetworking',         '~> 3.0.0'
  s.dependency 'JSONModel',            '~> 1.2.0'
  s.dependency 'JSONKit-NoWarning',    '~> 1.2'
  s.dependency 'ReactiveCocoa',        '~> 2.5'
  s.dependency 'FMDB',                 '~> 2.5'
  s.dependency 'DRDNetworking',        '~> 0.6.1'
end
