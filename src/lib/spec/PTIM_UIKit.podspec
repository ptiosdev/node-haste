Pod::Spec.new do |s|
  s.name             = "PTIM_UIKit"
  s.version          = "0.1.0"
  s.summary          = "PTIM_UIKit is IM UI Lib."

  s.homepage         = "https://bitbucket.org/ptdev/ptim_uikit"

  s.license          = 'MIT'
  s.author           = { "PTiOSDev" => "quyx@protruly.com.cn" }
  s.source           = { :git => "git@bitbucket.org:ptdev/ptim_uikit.git", :tag => s.version.to_s }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'PTIM_UIKit' => ['Pod/Assets/*.png']
  }

  s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
